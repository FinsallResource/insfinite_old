export class User_policy_vehicle_details{

    public brand:String;
    public model: String;
    public registrationYear: String;
    public registrationNumber: String;

    public getBrand(): String {
        return this.brand;
    }

    public setBrand(brand: String): void {
        this.brand = brand;
    }

    public getModel(): String {
        return this.model;
    }

    public setModel(model: String): void {
        this.model = model;
    }

    public getRegistrationYear(): String {
        return this.registrationYear;
    }

    public setRegistrationYear(registrationYear: String): void {
        this.registrationYear = registrationYear;
    }

    public getRegistrationNumber(): String {
        return this.registrationNumber;
    }

    public setRegistrationNumber(registrationNumber: String): void {
        this.registrationNumber = registrationNumber;
    }



}