import { User_policy_agent } from './User_policy_agent';
import { User_relation } from './User_relation';
import { User_policy_coverage } from './User_policy_coverage';
import { User_policy_document } from './User_policy_document';
import { External_entity_name } from './External_entity_name';
import { External_entity_type } from './External_entity_type';
import { User_policy_vehicle_details } from './user_policy_vehicle_details';

export class User_policy{
    private isArchive: string;

    private external_entity_name:External_entity_name ;

    private addedDate: String;

    private policyName: String;

    private user_relation: User_relation;

    private policyNumber: String;

    public policyEndDate: string;

    public policyStartDate: string;

    private  userPolicyId: String;

    private  paymentDisbursed: String;

    private  isFinsAll: String;

    private  policyPremium: String;

    private createdBy: String;

    public external_entity_type: External_entity_type;

    public user_policy_coverage:User_policy_coverage;

    private user_policy_agent:User_policy_agent;

    private policyDocumentPath: String;

    private status: String;

    private user_policy_document:User_policy_document[];

    public user_policy_vehicle_details:User_policy_vehicle_details;


    public getUser_policy_vehicle_details(): User_policy_vehicle_details {
        return this.user_policy_vehicle_details;
    }

    public setUser_policy_vehicle_details(user_policy_vehicle_details: User_policy_vehicle_details): void {
        this.user_policy_vehicle_details = user_policy_vehicle_details;
    }

    public getIsArchive(): string {
        return this.isArchive;
    }

    public setIsArchive(isArchive: string): void {
        this.isArchive = isArchive;
    }

    public getExternal_entity_name(): External_entity_name {
        return this.external_entity_name;
    }

    public setExternal_entity_name(external_entity_name: External_entity_name): void {
        this.external_entity_name = external_entity_name;
    }

    public getAddedDate(): String {
        return this.addedDate;
    }

    public setAddedDate(addedDate: String): void {
        this.addedDate = addedDate;
    }

    public getPolicyName(): String {
        return this.policyName;
    }

    public setPolicyName(policyName: String): void {
        this.policyName = policyName;
    }

    public getUser_relation(): User_relation {
        return this.user_relation;
    }

    public setUser_relation(user_relation: User_relation): void {
        this.user_relation = user_relation;
    }

    public getPolicyNumber(): String {
        return this.policyNumber;
    }

    public setPolicyNumber(policyNumber: String): void {
        this.policyNumber = policyNumber;
    }

    public getPolicyEndDate(): string {
        return this.policyEndDate;
    }

    public setPolicyEndDate(policyEndDate: string): void {
        this.policyEndDate = policyEndDate;
    }

    public getPolicyStartDate(): string {
        return this.policyStartDate;
    }

    public setPolicyStartDate(policyStartDate: string): void {
        this.policyStartDate = policyStartDate;
    }

    public getUserPolicyId(): String {
        return this.userPolicyId;
    }

    public setUserPolicyId(userPolicyId: String): void {
        this.userPolicyId = userPolicyId;
    }

    public getPaymentDisbursed(): String {
        return this.paymentDisbursed;
    }

    public setPaymentDisbursed(paymentDisbursed: String): void {
        this.paymentDisbursed = paymentDisbursed;
    }

    public getIsFinsAll(): String {
        return this.isFinsAll;
    }

    public setIsFinsAll(isFinsAll: String): void {
        this.isFinsAll = isFinsAll;
    }

    public getPolicyPremium(): String {
        return this.policyPremium;
    }

    public setPolicyPremium(policyPremium: String): void {
        this.policyPremium = policyPremium;
    }

    public getCreatedBy(): String {
        return this.createdBy;
    }

    public setCreatedBy(createdBy: String): void {
        this.createdBy = createdBy;
    }

    public getExternal_entity_type(): External_entity_type {
        return this.external_entity_type;
    }

    public setExternal_entity_type(external_entity_type: External_entity_type): void {
        this.external_entity_type = external_entity_type;
    }

    public getUser_policy_coverage(): User_policy_coverage {
        return this.user_policy_coverage;
    }

    public setUser_policy_coverage(user_policy_coverage: User_policy_coverage): void {
        this.user_policy_coverage = user_policy_coverage;
    }

    public getUser_policy_agent(): User_policy_agent {
        return this.user_policy_agent;
    }

    public setUser_policy_agent(user_policy_agent: User_policy_agent): void {
        this.user_policy_agent = user_policy_agent;
    }

    public getPolicyDocumentPath(): String {
        return this.policyDocumentPath;
    }

    public setPolicyDocumentPath(policyDocumentPath: String): void {
        this.policyDocumentPath = policyDocumentPath;
    }

    public getStatus(): String {
        return this.status;
    }

    public setStatus(status: String): void {
        this.status = status;
    }

    public getUser_policy_document(): User_policy_document[] {
        return this.user_policy_document;
    }

    public setUser_policy_document(user_policy_document: User_policy_document[]): void {
        this.user_policy_document = user_policy_document;
    }

}