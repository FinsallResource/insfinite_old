import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { RequestVehicleBrand } from './model/RequestVehicleBrand';
import { LoginComponent } from './login/login.component';
import { BaseService } from './base.service';
import { RequestVehicleModel } from './model/RequestVehicleModel';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {


  constructor(private http: HttpClient,private requestBrand: RequestVehicleBrand,
    private loginComponent: LoginComponent,
    private requestModel:RequestVehicleModel,private baseService: BaseService) { }

  //baseURL: string = "https://cors-anywhere.herokuapp.com/https://www.finsall.co.in/FinsAllServer/api";

  //baseURL: string = 'https://www.finsall.co.in/FinsAllServer/api';

  
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
 .set('authentication-token',this.loginComponent.sendResponseLogin().getPassword().toString())
  .set('authentication-username',this.loginComponent.sendResponseLogin().getUserId().valueOf().toString())
   };

   getVehicleBrand(identifier: string)
   {
    this.requestBrand.setClientId(this.loginComponent.sendResponseLogin().getClientId());
    this.requestBrand.setLoggedInUserId(this.loginComponent.sendResponseLogin().getUserId());
    this.requestBrand.setVersion(this.loginComponent.sendResponseLogin().getVersion());
    this.requestBrand.setServiceName("FinsAllService");
    this.requestBrand.setServiceMethod("getVehicleBrandsById");
    this.requestBrand.setGenericIdentifier(identifier);
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.requestBrand),this.options);
   }

   getVehicleType(brandId: String)
   {
    this.requestModel.setClientId(this.loginComponent.sendResponseLogin().getClientId());
    this.requestModel.setLoggedInUserId(this.loginComponent.sendResponseLogin().getUserId());
    this.requestModel.setServiceMethod("getVehicleModelsByBrandId");
    this.requestModel.setServiceName("FinsAllService");
    this.requestModel.setVersion(this.loginComponent.sendResponseLogin().getVersion());
    this.requestModel.setGenericIdentifier(brandId);
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.requestModel),this.options);

   }

}
