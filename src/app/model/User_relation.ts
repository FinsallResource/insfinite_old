export class User_relation{
    private firstName:string ;

    private lastName:string;

    private userRelationId:string;

    private dob:string;

    private relation:string;

    public getFirstName(): string {
        return this.firstName;
    }

    public setFirstName(firstName: string): void {
        this.firstName = firstName;
    }

    public getLastName(): string {
        return this.lastName;
    }

    public setLastName(lastName: string): void {
        this.lastName = lastName;
    }

    public getUserRelationId(): string {
        return this.userRelationId;
    }

    public setUserRelationId(userRelationId: string): void {
        this.userRelationId = userRelationId;
    }

    public getDob(): string {
        return this.dob;
    }

    public setDob(dob: string): void {
        this.dob = dob;
    }

    public getRelation(): string {
        return this.relation;
    }

    public setRelation(relation: string): void {
        this.relation = relation;
    }

}