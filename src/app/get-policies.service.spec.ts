import { TestBed } from '@angular/core/testing';

import { GetPoliciesService } from './get-policies.service';

describe('GetPoliciesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetPoliciesService = TestBed.get(GetPoliciesService);
    expect(service).toBeTruthy();
  });
});
