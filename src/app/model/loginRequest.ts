export class LoginRequest{
    private  password:String;

    private  clientId:String;

    private  isPassword:boolean;

    private roles: String;

    private serviceName:String;

    private genericParam:String;

    private version:String;

    private serviceMethod:String;

    public getPassword(): String {
        return this.password;
    }

    public setPassword(password: String): void {
        this.password = password;
    }

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getIsPassword(): boolean{
        return this.isPassword;
    }

    public setIsPassword(isPassword: boolean): void {
        this.isPassword = isPassword;
    }

    public getRoles(): String {
        return this.roles;
    }

    public setRoles(roles: String): void {
        this.roles = roles;
    }

    public getServiceName(): String {
        return this.serviceName;
    }

    public setServiceName(serviceName: String): void {
        this.serviceName = serviceName;
    }

    public getGenericParam(): String {
        return this.genericParam;
    }

    public setGenericParam(genericParam: String): void {
        this.genericParam = genericParam;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

    public getServiceMethod(): String {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: String): void {
        this.serviceMethod = serviceMethod;
    }

}