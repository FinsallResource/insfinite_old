import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginComponent } from './login/login.component';
import {LoginResponse} from './model/loginResponse';

//-------------------------Created by Suparno Saha-------------------------------//
//this service acts as a authentication guard so that on page reload it throws out to login page
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
        // declare variables
        private responseLogin:LoginResponse,
        private router:Router
  ) {}

    canActivate(
     route: ActivatedRouteSnapshot,
     state: RouterStateSnapshot,

  ): boolean {
        // logic that determines true or false
          if(this.responseLogin.getUserId())
          return true;
          else
          {
           this.router.navigate(["login"]);
            return false;
          }
          
  }
}