import { Component, OnInit, ViewChild, HostListener,OnDestroy,ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource,MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { LoginResponse } from '../model/loginResponse';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import {MediaMatcher} from '@angular/cdk/layout';
import { GetPoliciesService } from '../get-policies.service';
import { ResponsePolicies } from '../model/ResponsePolicies';
import { User_policy } from '../model/User_policy';
import { PolicyDataService } from '../policy-data.service';
import { PolicyDetailsComponent } from '../policy-details/policy-details.component';
import { ArchiveService } from '../archive.service';
import { AddPolicyComponent } from '../add-policy/add-policy.component';
import { SharePolicyComponent } from '../share-policy/share-policy.component';

@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.css']
})
export class PoliciesComponent implements OnInit {

  constructor(public dialog: MatDialog,private router: Router,
    private loginComponent:LoginComponent,private responseLogin:LoginResponse,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,private service:GetPoliciesService,
    private responsePolicies:ResponsePolicies, private userPolicy: User_policy,
    private policyData:PolicyDataService, private archive:ArchiveService,public snackbar:MatSnackBar
    ) {
      this.mobileQuery = media.matchMedia('(max-width: 1440px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
     }

     private _mobileQueryListener: () => void;
    mobileQuery: MediaQueryList;
    isDataAvailable:boolean = false;
    loading=false;
    i: number;
    isErrorMessage= false;
    response: string[]=[];
    dataSource = new MatTableDataSource(this.response);
    errorMessage:String="";
    archiveMessage:string="";
    
  
  @HostListener('window:popstate', ['$event'])
 //this method is for opening a popup on pressing browser back button
 onPopState(event:any) {
  
    this.openDialog1("You will be logged out. Do you wish to continue?");
  }
  


//Dialog box to get confirmation if user wants to log out on pressing back 
openDialog1(data:string): void {
  this.ngOnInit();
   const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
     width: '350px',
     data: data
   });
   dialogRef.afterClosed().subscribe(result => {
     if(result) {
     
       // DO SOMETHING
       this.logOut();
     }
   });
 }

 policyDetails(policy: any)
 {
    this.policyData.policy=policy;
    this.router.navigate(["policyDetails"]);
 }
 logOut()
 {
   this.responseLogin.setUserId(undefined);
   this.responseLogin.setErrorMessage("Logged out Successfully");
   this.loginComponent.getResponseLogin(this.responseLogin);
   
   this.router.navigate(["login"]);
 }

 archiveDialog(policy:any): void{
  const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    width: '350px',
    data: "Do you want to Archive this policy?"
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
    
      this.archive.archivePolicy(policy).subscribe((data:any)=>
 {
   console.log(data);
   if(data.successMessage)
   this.archiveMessage=data.successMessage;
   else
   this.archiveMessage=data.errorMessage;
   this.openSnackBar(this.archiveMessage,"ok");
   this.ngOnInit();
 }
 );
    }
  });
 }

 openSnackBar(message: string, action: string) {
  this.snackbar.open(message, action, {
     duration: 5000,
  });
}
 openDialog(policy: any): void {
  this.policyData.policy=policy;
  const dialogRef = this.dialog.open(PolicyDetailsComponent, {
    panelClass: 'my-dialog',
    width: '650px',
    data: this.policyData.policy
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
    
      // DO SOMETHING
      
    }
  });
}

addPolicy():void{
  const dialogRef = this.dialog.open(AddPolicyComponent, {
    panelClass: 'my-dialog',
    width: 'auto',
    data: 'text'
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
    
      // DO SOMETHING
      
    }
  });
}

sharePolicy():void{
  const dialogRef = this.dialog.open(SharePolicyComponent, {
    panelClass: 'my-dialog',
    width: '500px',
    data: 'text'
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
    
      // DO SOMETHING
      
    }
  });
}

  //Used for loading the data in variables before the page gets loaded. Called inside onInit()
  fetchEvent(): Promise<any> {
    return this.service.getUserPolicies().then((data:any)=>
    {
      
      if(data.errorMessage)
      {
        this.isErrorMessage=true;
      this.errorMessage=data.errorMessage;
      }
      else{
        this.isErrorMessage=false;
       
      this.responsePolicies=data;
      console.log(data);
      this.userPolicy=data.user_policy;
      this.response=data.user_policy;  
      
    }});
}

  ngOnInit() {
    this.loading=true;
    window.history.pushState( {} , 'policies','#/policies');
    this.fetchEvent().then(()=>
    {this.isDataAvailable = true;
      this.loading=false;
    })
  }

  

}
