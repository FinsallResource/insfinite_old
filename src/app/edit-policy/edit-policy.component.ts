import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar} from '@angular/material';
import { PolicyDetailsComponent } from '../policy-details/policy-details.component';
import { PolicyDataService } from '../policy-data.service';
import { User_policy } from '../model/User_policy';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { VehicleService } from '../vehicle.service';
import { Vehicle_brand } from '../model/Vehicle_brand';

@Component({
  selector: 'app-edit-policy',
  templateUrl: './edit-policy.component.html',
  styleUrls: ['./edit-policy.component.css']
})
export class EditPolicyComponent implements OnInit {

  vehicleBrands:Vehicle_brand[];
  vehicleTypes:string[]=[];
  vehicleMaker:string[]=[];
  brandId:string;
  vehicleModels:Vehicle_model[];
  loading:boolean=false;
  coverage:string[]=[];
  i: number;

  constructor( public dialogRef: MatDialogRef<PolicyDetailsComponent>,private policydata:PolicyDataService, 
    @Inject(MAT_DIALOG_DATA) public data:User_policy,public dialog: MatDialog,private router: Router,
    private userPolicy:User_policy,private vehicleService:VehicleService)
    {
      
      if(this.data.external_entity_type.externalEntityTypeId== '1' || this.data.external_entity_type.externalEntityTypeId== '2')
      {
        this.loading=true;
      this.vehicleService.getVehicleBrand(this.data.external_entity_type.externalEntityTypeId).subscribe((data:any)=>
      {
        this.vehicleBrands=data.vehicle_brand;
        console.log(this.vehicleBrands);
        var i= 0;
        while(i<this.vehicleBrands.length)
        {
          if(this.vehicleBrands[i].brand=== this.data.user_policy_vehicle_details.brand)
            {
              this.brandId=this.vehicleBrands[i].vehicleBrandId;
              console.log(this.brandId+"---------"+this.data.user_policy_vehicle_details.model+"--------"+this.vehicleBrands[i].brand);
              }
              this.vehicleMaker[i]=this.vehicleBrands[i].brand;
              i++;
        }
        this.vehicleService.getVehicleType(this.brandId).subscribe((data1:any)=>
        {
          console.log(data1);
        this.vehicleModels=data1.vehicle_model;
        i=0;
        while(i<this.vehicleModels.length)
        {this.vehicleTypes[i]=this.vehicleModels[i].model;
        i++;}
        this.loading=false;
      }
          )
      })
      if(data.user_policy_coverage.coverageType != undefined)
      {
        this.loading=true;
        this.coverage= data.user_policy_coverage.coverageType.split(", ");
        console.log(this.coverage);
        console.log(this.coverage.includes("Comprehensive"));
        this.loading=false;
      }
    }
    }

    onNoClick(): void {
      this.dialogRef.close();}

      
  isComprehensive(event:any){
    if(event.target.checked)
    {
      this.coverage.push("Comprehensive");
    }
    else
    {
      this.i=this.coverage.indexOf("Comprehensive");
      this.coverage.splice(this.i,1);
    }
  }

  isThirdParty(event:any){
    if(event.target.checked)
    {
      this.coverage.push("Third Party");
    }
    else
    {
      this.i=this.coverage.indexOf("Third Party");
      this.coverage.splice(this.i,1);
    }
  }

  isReplacement(event:any){
    if(event.target.checked)
    {
      this.coverage.push("Replacement Cover");
    }
    else
    {
      this.i=this.coverage.indexOf("Replacement Cover");
      this.coverage.splice(this.i,1);
    }
  }

  ngOnInit() {

 
  }

}
