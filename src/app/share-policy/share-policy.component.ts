import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-share-policy',
  templateUrl: './share-policy.component.html',
  styleUrls: ['./share-policy.component.css']
})
export class SharePolicyComponent implements OnInit {
  errorMessage: string=null;

  constructor( public dialogRef: MatDialogRef<SharePolicyComponent>,
    @Inject(MAT_DIALOG_DATA) public data:string[],public dialog: MatDialog,private router: Router, 
     private loginComponent:LoginComponent,public snackbar:MatSnackBar)
    {    }

    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
    excelData:string[];
    mobileNumbers:string[]=[];
    ready:string;
    onFileChange(evt: any) {
      this.errorMessage=null;
      this.ready="n";
      /* wire up file reader */
      const target: DataTransfer = <DataTransfer>(evt.target);
      if (target.files.length !== 1) throw new Error('Cannot use multiple files');
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
  
        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
        /* save data */
        this.excelData = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
        console.log(this.excelData);
        let i=0;
        let j=0;
        for(let event of this.excelData)
        {
            if(i==0)
            i++;
            else
            {
              if(parseInt(event)<=9999999999 && parseInt(event)>999999999)
               { this.mobileNumbers[j]=event[0];
                j++;}
                else
                {this.errorMessage="One or more numbers are not in correct format.";
                break;}
            }
        }
        if(this.errorMessage == null)
        this.ready="y";
        console.log(this.mobileNumbers);
      };
      reader.readAsBinaryString(target.files[0]);
    }

    onNoClick(): void {
      this.dialogRef.close();}

  ngOnInit() {
  }

}
