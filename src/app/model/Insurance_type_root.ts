export class Insurance_type_root{
    private name: string;

    private insuranceTypeRootId: string;

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getInsuranceTypeRootId(): string {
        return this.insuranceTypeRootId;
    }

    public setInsuranceTypeRootId(insuranceTypeRootId: string): void {
        this.insuranceTypeRootId = insuranceTypeRootId;
    }

}