export class User_policy_agent{
   
    private userPolicyAgentId:string;

    private addedDate:string;

    private phone:string;

    private name:string;

    public getUserPolicyAgentId(): string {
        return this.userPolicyAgentId;
    }

    public setUserPolicyAgentId(userPolicyAgentId: string): void {
        this.userPolicyAgentId = userPolicyAgentId;
    }

    public getAddedDate(): string {
        return this.addedDate;
    }

    public setAddedDate(addedDate: string): void {
        this.addedDate = addedDate;
    }

    public getPhone(): string {
        return this.phone;
    }

    public setPhone(phone: string): void {
        this.phone = phone;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

}