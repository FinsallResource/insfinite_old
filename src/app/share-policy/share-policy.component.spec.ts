import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharePolicyComponent } from './share-policy.component';

describe('SharePolicyComponent', () => {
  let component: SharePolicyComponent;
  let fixture: ComponentFixture<SharePolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharePolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharePolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
