import { Injectable } from '@angular/core';
import { User_policy } from './model/User_policy';

@Injectable({
  providedIn: 'root'
})
export class PolicyDataService {

  constructor() { }

   policy:User_policy;

}
