import { Injectable } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService } from './base.service';
import { ArchivePolicy } from './model/ArchivePolicy';

@Injectable({
  providedIn: 'root'
})
export class ArchiveService {

  constructor(private loginComponent: LoginComponent,private http: HttpClient,
    private baseService: BaseService, private request:ArchivePolicy) { }

  element: any;
 
 
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
  .set('authentication-token',this.loginComponent.sendResponseLogin().getPassword().toString())
  .set('authentication-username',this.loginComponent.sendResponseLogin().getUserId().valueOf().toString())
   };

   archivePolicy(policy: any)
   {
    this.request=new ArchivePolicy();
    this.request.setServiceName("UserPolicyService");
    this.request.setServiceMethod("doArchivePolicy");
    this.request.setClientId(this.loginComponent.sendResponseLogin().getClientId());
    this.request.setVersion(this.loginComponent.sendResponseLogin().getVersion());
    this.request.setLoggedInUserId(this.loginComponent.sendResponseLogin().getUserId());
    this.request.setRoles(this.loginComponent.sendResponseLogin().getRoles());
    this.request.setIsShared(policy.isShared);
    this.request.setUserPolicyId(policy.userPolicyId);
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.request),this.options);
   }

   getArchive()
   {
     this.request=new ArchivePolicy();
     this.request.setServiceName("UserPolicyService");
    this.request.setServiceMethod("getArchivePolicy");
    this.request.setClientId(this.loginComponent.sendResponseLogin().getClientId());
    this.request.setVersion(this.loginComponent.sendResponseLogin().getVersion());
    this.request.setLoggedInUserId(this.loginComponent.sendResponseLogin().getUserId());
    this.request.setRoles(this.loginComponent.sendResponseLogin().getRoles());
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.request),this.options).toPromise();
   }
}
