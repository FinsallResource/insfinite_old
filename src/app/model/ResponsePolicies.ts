import { User_policy } from './User_policy';

export class ResponsePolicies{
    private  user_policy: User_policy[];

    private clientId:String;

    private version:String;

    public getUser_policy(): User_policy[] {
        return this.user_policy;
    }

    public setUser_policy(user_policy: User_policy[]): void {
        this.user_policy = user_policy;
    }

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }


}