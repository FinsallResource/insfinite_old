import { TestBed } from '@angular/core/testing';

import { ExternalEntityService } from './external-entity.service';

describe('ExternalEntityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExternalEntityService = TestBed.get(ExternalEntityService);
    expect(service).toBeTruthy();
  });
});
