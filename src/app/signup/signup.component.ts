import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginResponse } from '../model/loginResponse';
import { Router } from '@angular/router';
import { AuthGuardService } from '../auth-guard.service';
import { LocationStrategy } from '@angular/common';
import { BaseService } from '../base.service';
import { ValidateLoginService } from '../validate-login.service';
import { sha256 } from 'js-sha256';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  constructor(private formBuilder: FormBuilder,
    private response: LoginResponse, private router:Router,private auth:AuthGuardService,
    private locationStrategy: LocationStrategy,
    private baseService:BaseService,
    private validate: ValidateLoginService) {}

  mobileNo: string= "";
 password: string= "";
 isValid: boolean= false; 
 clientId: string="2017";
 isRequired: boolean=false;
 errorMessage:String='';
 isLoggedOut: boolean=false;
 loading = false;
 roles: String= "customer";
 key:string=this.baseService.key;
 isCaptchaValid:boolean=false;
 registerForm: FormGroup;
 
 UsernameFormControl = new FormControl('', [
  Validators.required,
  Validators.pattern("([_a-z0-9]+(\\.[_a-z0-9]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,5}))|(\\d+$)$"),
]);

PasswordFormControl = new FormControl('', [
  Validators.required,]);

 fillRequired()
 {
this.isRequired=true;
 }
//restricts browser back key from getting into previous page 
 @HostListener('window:popstate', ['$event'])
  onPopState(event:any) {
    this.router.navigate(["login"]);
    
  }

 onEnteringMobileNo(event: any)
 {
   this.mobileNo=event.target.value;
   
   this.isRequired=false;
 }

 onEnteringPassword(event: any)
 {
   this.password=event.target.value;
   this.isRequired=false;
   console.log(this.password);
   this.password=(sha256.hmac(this.key, this.password));
   console.log(this.password);
  
   
 }



//method get called on button press (call the login API)
 check(){
         //this.response=this.validate.validateWithMobileNo(this.mobileNo,this.password);
         this.loading=true;
         
         if(this.isCaptchaValid)
         this.validate.validate(this.mobileNo,this.password).subscribe((data:any) => 
      {
        
        this.response.setClientId((data.clientId));
        this.response.setPassword((data.password));
        this.response.setVersion((data.version));
        this.response.setUserId((data.userId));
        this.response.setUserName((data.userName));
        this.response.setLastName((data.lastName));
        this.response.setFirstName((data.firstName));
        this.response.setEmailId((data.emailId));
        this.response.setMobileNo((data.mobileNo));
        this.response.setPan((data.pan));
        this.response.setRoleId((data.roleId));
        this.response.setCountry((data.country));
        this.response.setErrorMessage((data.errorMessage));
        this.response.setRoles(data.roles);
        this.loading=false;
        

       if (this.response.getErrorMessage() == undefined)
     {
       
      this.router.navigate(['applicationDetails']);
      }
      else{
        
        this.errorMessage=this.response.getErrorMessage();
        this.isValid=true;
      }
      
     },error => console.debug('ERROR', error) )
    
     else
     {this.errorMessage="Please Validate Captcha to Log in."
     this.isValid=true;
     this.loading=false;}
     
        }

  sendResponseLogin(){
    return this.response;
  }
 getResponseLogin(res:LoginResponse){

  this.response=res;
  this.isLoggedOut=true;
 }

 preventBackButton() {
  history.pushState(null, null, location.href);
  this.locationStrategy.onPopState(() => {
    history.pushState(null, null, location.href);
  })
}
  
get f() { return this.registerForm.controls; }

  ngOnInit() {
   
  }

}

