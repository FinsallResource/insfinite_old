export class ArchivePolicy{

    private clientId:string;

    private roles:string;

    private userPolicyId:string;

    private loggedInUserId:string;

    private serviceName:string;

    private version:string;

    private isShared:string;

    private serviceMethod:string;

    public getClientId(): string {
        return this.clientId;
    }

    public setClientId(clientId: string): void {
        this.clientId = clientId;
    }

    public getRoles(): string {
        return this.roles;
    }

    public setRoles(roles: string): void {
        this.roles = roles;
    }

    public getUserPolicyId(): string {
        return this.userPolicyId;
    }

    public setUserPolicyId(userPolicyId: string): void {
        this.userPolicyId = userPolicyId;
    }

    public getLoggedInUserId(): string {
        return this.loggedInUserId;
    }

    public setLoggedInUserId(loggedInUserId: string): void {
        this.loggedInUserId = loggedInUserId;
    }

    public getServiceName(): string {
        return this.serviceName;
    }

    public setServiceName(serviceName: string): void {
        this.serviceName = serviceName;
    }

    public getVersion(): string {
        return this.version;
    }

    public setVersion(version: string): void {
        this.version = version;
    }

    public getIsShared(): string {
        return this.isShared;
    }

    public setIsShared(isShared: string): void {
        this.isShared = isShared;
    }

    public getServiceMethod(): string {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: string): void {
        this.serviceMethod = serviceMethod;
    }

}