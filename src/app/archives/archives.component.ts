import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ArchiveService } from '../archive.service';
import { User_policy_document } from '../model/User_policy_document';
import { User_policy } from '../model/User_policy';
import {MediaMatcher} from '@angular/cdk/layout';
import { PolicyDataService } from '../policy-data.service';
import { MatDialog} from '@angular/material'
import { PolicyDetailsComponent } from '../policy-details/policy-details.component';

@Component({
  selector: 'app-archives',
  templateUrl: './archives.component.html',
  styleUrls: ['./archives.component.css']
})
export class ArchivesComponent implements OnInit {
  isDataAvailable: boolean;
  loading: boolean;
  private _mobileQueryListener: () => void;
    mobileQuery: MediaQueryList;
  constructor(private service:ArchiveService, private userPolicy:User_policy, media: MediaMatcher,
    changeDetectorRef: ChangeDetectorRef,private policyData:PolicyDataService,
    public dialog: MatDialog) { 
    this.mobileQuery = media.matchMedia('(max-width: 1440px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  isErrorMessage:boolean;
  errorMessage:string;
  response:string[];

  openDialog(policy: any): void {
    this.policyData.policy=policy;
    const dialogRef = this.dialog.open(PolicyDetailsComponent, {
      panelClass: 'my-dialog',
      width: '650px',
      data: this.policyData.policy
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
      
        // DO SOMETHING
        
      }
    });
  }

//Used for loading the data in variables before the page gets loaded. Called inside onInit()
fetchArchives(){
  this.loading=true;
  return this.service.getArchive().then((data:any)=>
  {
    if(data.errorMessage)
    {
      this.isErrorMessage=true;
    this.errorMessage=data.errorMessage;
    }
    else{
      this.isErrorMessage=false;
     
    // this.responsePolicies=data;
    console.log(data);
     this.userPolicy=data.user_policy;
     this.response=data.user_policy;  
    
  }});
}


  ngOnInit() {
    this.fetchArchives().then((data:any)=>
    {
      this.isDataAvailable = true;
      this.loading=false;
    }
    )
  }

}
