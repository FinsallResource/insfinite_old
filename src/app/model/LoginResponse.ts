//-------------------------Created by Suparno Saha-------------------------------//
export class LoginResponse
{
    private lastName:string;

    private country:string;

    private kycStatus:string;

    private occupation:string;

    private annualIncome:string;

    private gender:string;

    private city:string;

    private roles:string;

    private emailId:string;

    private password:string;

    private isCorporate:string;

    private addressLine1:string;

    private aadhar:string;

    private addressLine2:string;

    private notifyMobile:string;

    private pan:string;

    private uniqueIdentifier:string;

    private clientId: string;

    private roleId:string;

    private mobileNo:string;

    private userName:string;

    private version:string;

    private userId:string;

    private firstName:string;

    private dob:string;

    private pinCode:string;

    private notifyEmail:string;

    private status:string;

    private errorMessage: string;

    private statusCode: string;

    public getErrorMessage(): string {
        return this.errorMessage;
    }

    public setErrorMessage(errorMessage: string): void {
        this.errorMessage = errorMessage;
    }

    public getStatusCode(): string {
        return this.statusCode;
    }

    public setStatusCode(statusCode: string): void {
        this.statusCode = statusCode;
    }


    public getLastName(): string {
        return this.lastName;
    }

    public setLastName(lastName: string): void {
        this.lastName = lastName;
    }

    public getCountry(): string {
        return this.country;
    }

    public setCountry(country: string): void {
        this.country = country;
    }

    public getKycStatus(): string {
        return this.kycStatus;
    }

    public setKycStatus(kycStatus: string): void {
        this.kycStatus = kycStatus;
    }

    public getOccupation(): string {
        return this.occupation;
    }

    public setOccupation(occupation: string): void {
        this.occupation = occupation;
    }

    public getAnnualIncome(): string {
        return this.annualIncome;
    }

    public setAnnualIncome(annualIncome: string): void {
        this.annualIncome = annualIncome;
    }

    public getGender(): string {
        return this.gender;
    }

    public setGender(gender: string): void {
        this.gender = gender;
    }

    public getCity(): string {
        return this.city;
    }

    public setCity(city: string): void {
        this.city = city;
    }

    public getRoles(): string {
        return this.roles;
    }

    public setRoles(roles: string): void {
        this.roles = roles;
    }

    public getEmailId(): string {
        return this.emailId;
    }

    public setEmailId(emailId: string): void {
        this.emailId = emailId;
    }

    public getPassword(): string {
        return this.password;
    }

    public setPassword(password: string): void {
        this.password = password;
    }

    public getIsCorporate(): string {
        return this.isCorporate;
    }

    public setIsCorporate(isCorporate: string): void {
        this.isCorporate = isCorporate;
    }

    public getAddressLine1(): string {
        return this.addressLine1;
    }

    public setAddressLine1(addressLine1: string): void {
        this.addressLine1 = addressLine1;
    }

    public getAadhar(): string {
        return this.aadhar;
    }

    public setAadhar(aadhar: string): void {
        this.aadhar = aadhar;
    }

    public getAddressLine2(): string {
        return this.addressLine2;
    }

    public setAddressLine2(addressLine2: string): void {
        this.addressLine2 = addressLine2;
    }

    public getNotifyMobile(): string {
        return this.notifyMobile;
    }

    public setNotifyMobile(notifyMobile: string): void {
        this.notifyMobile = notifyMobile;
    }

    public getPan(): string {
        return this.pan;
    }

    public setPan(pan: string): void {
        this.pan = pan;
    }

    public getUniqueIdentifier(): string {
        return this.uniqueIdentifier;
    }

    public setUniqueIdentifier(uniqueIdentifier: string): void {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public getClientId(): string {
        return this.clientId;
    }

    public setClientId(clientId: string): void {
        this.clientId = clientId;
    }

    public getRoleId(): string {
        return this.roleId;
    }

    public setRoleId(roleId: string): void {
        this.roleId = roleId;
    }

    public getMobileNo(): string {
        return this.mobileNo;
    }

    public setMobileNo(mobileNo: string): void {
        this.mobileNo = mobileNo;
    }

    public getUserName(): string {
        return this.userName;
    }

    public setUserName(userName: string): void {
        this.userName = userName;
    }

    public getVersion(): string {
        return this.version;
    }

    public setVersion(version: string): void {
        this.version = version;
    }

    public getUserId(): string {
        return this.userId;
    }

    public setUserId(userId: string): void {
        this.userId = userId;
    }

    public getFirstName(): string {
        return this.firstName;
    }

    public setFirstName(firstName: string): void {
        this.firstName = firstName;
    }

    public getDob(): string {
        return this.dob;
    }

    public setDob(dob: string): void {
        this.dob = dob;
    }

    public getPinCode(): string {
        return this.pinCode;
    }

    public setPinCode(pinCode: string): void {
        this.pinCode = pinCode;
    }

    public getNotifyEmail(): string {
        return this.notifyEmail;
    }

    public setNotifyEmail(notifyEmail: string): void {
        this.notifyEmail = notifyEmail;
    }

    public getStatus(): string {
        return this.status;
    }

    public setStatus(status: string): void {
        this.status = status;
    }
}