import { Insurance_type_root } from './Insurance_type_root';

export class Insurance_type_and_root{
    private insurance_type_root:Insurance_type_root;

    public getInsurance_type_root(): Insurance_type_root {
        return this.insurance_type_root;
    }

    public setInsurance_type_root(insurance_type_root: Insurance_type_root): void {
        this.insurance_type_root = insurance_type_root;
    }

}