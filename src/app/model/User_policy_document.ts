export class User_policy_document{
    private documentPath: string;

    public getDocumentPath(): string {
        return this.documentPath;
    }

    public setDocumentPath(documentPath: string): void {
        this.documentPath = documentPath;
    }

}