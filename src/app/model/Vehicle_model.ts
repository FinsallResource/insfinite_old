class Vehicle_model
{
    public vehicleModelId:string;

    public model: string;

    public getVehicleModelId(): string {
        return this.vehicleModelId;
    }

    public setVehicleModelId(vehicleModelId: string): void {
        this.vehicleModelId = vehicleModelId;
    }

    public getModel(): string {
        return this.model;
    }

    public setModel(model: string): void {
        this.model = model;
    }

}