import { Insurance_type_and_root } from './Insurance_type_and_root';

export class Insurance_type{
    private insuranceTypeId:string;

    private insurance_type_and_root: Insurance_type_and_root[] ;

    private type:string;

    public getInsuranceTypeId(): string {
        return this.insuranceTypeId;
    }

    public setInsuranceTypeId(insuranceTypeId: string): void {
        this.insuranceTypeId = insuranceTypeId;
    }

    public getInsurance_type_and_root(): Insurance_type_and_root[] {
        return this.insurance_type_and_root;
    }

    public setInsurance_type_and_root(insurance_type_and_root: Insurance_type_and_root[]): void {
        this.insurance_type_and_root = insurance_type_and_root;
    }

    public getType(): string {
        return this.type;
    }

    public setType(type: string): void {
        this.type = type;
    }


   

    


}