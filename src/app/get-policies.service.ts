import { Injectable } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService } from './base.service';
import { RequestApplication } from './model/requestApplication';

@Injectable({
  providedIn: 'root'
})
export class GetPoliciesService {

  constructor(private loginComponent: LoginComponent,private http: HttpClient,
    private baseService: BaseService, private request:RequestApplication) { }

  element: any;
 
 
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
  .set('authentication-token',this.loginComponent.sendResponseLogin().getPassword().toString())
  .set('authentication-username',this.loginComponent.sendResponseLogin().getUserId().valueOf().toString())
   };
  
   getUserPolicies()
   {
    this.request.setServiceName("UserPolicyService");
    this.request.setServiceMethod("getUserPolicy");
    this.request.setClientId(this.loginComponent.sendResponseLogin().getClientId());
    this.request.setVersion(this.loginComponent.sendResponseLogin().getVersion());
    this.request.setLoggedInUserId(this.loginComponent.sendResponseLogin().getUserId());
    this.request.setRoles(this.loginComponent.sendResponseLogin().getRoles());
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.request),this.options).toPromise();
   }
}
