export class Vehicle_brand
{
    public vehicleBrandId: string;

    public brand: string;

    public getVehicleBrandId(): String {
        return this.vehicleBrandId;
    }

    public setVehicleBrandId(vehicleBrandId: string): void {
        this.vehicleBrandId = vehicleBrandId;
    }

    public getBrand(): string {
        return this.brand;
    }

    public setBrand(brand: string): void {
        this.brand = brand;
    }

}