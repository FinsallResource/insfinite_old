import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { PoliciesComponent } from './policies/policies.component';
import { AuthGuardService } from './auth-guard.service';
import { PolicyDetailsComponent } from './policy-details/policy-details.component';
import { AddPolicyComponent } from './add-policy/add-policy.component';
import { ArchivesComponent } from './archives/archives.component';
import { EditPolicyComponent } from './edit-policy/edit-policy.component';



const routes: Routes = [ {path: '', pathMatch:'full',redirectTo:'login'},
{path: 'login',component: LoginComponent},
{path: 'signup',component:SignupComponent},
{path: 'policies',component:PoliciesComponent},
{path: 'policyDetails',component:PolicyDetailsComponent},
{path: 'addpolicy',component:AddPolicyComponent},
{path: 'archives',component:ArchivesComponent},
{path: 'editpolicy',component:EditPolicyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
