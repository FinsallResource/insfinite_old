import { Injectable } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService } from './base.service';
import { RequestApplication } from './model/requestApplication';
import { RequestEntityName } from './model/RequestEntityName';

@Injectable({
  providedIn: 'root'
})
export class ExternalEntityService {

  constructor(private loginComponent: LoginComponent,private http: HttpClient,
    private baseService: BaseService, private request:RequestApplication,
    private entityNameRequest:RequestEntityName) { }


  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
  .set('authentication-token',this.loginComponent.sendResponseLogin().getPassword().toString())
  .set('authentication-username',this.loginComponent.sendResponseLogin().getUserId().valueOf().toString())
   };

   getExternalType(){
     this.request=new RequestApplication();
     this.request.setServiceName("FinsAllService");
    this.request.setServiceMethod("getExternalEntityType");
    this.request.setClientId(this.loginComponent.sendResponseLogin().getClientId());
    this.request.setVersion(this.loginComponent.sendResponseLogin().getVersion());
    this.request.setLoggedInUserId(this.loginComponent.sendResponseLogin().getUserId());
    this.request.setRoles(this.loginComponent.sendResponseLogin().getRoles());
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.request),this.options);

   }

   getExternalName(root: string[]){
    this.entityNameRequest.setServiceName("FinsAllService");
    this.entityNameRequest.setServiceMethod("getEntityTypeAndNameById");
    this.entityNameRequest.setGenericIdentifiers(root);
    this.entityNameRequest.setClientId(this.loginComponent.sendResponseLogin().getClientId());
    this.entityNameRequest.setVersion(this.loginComponent.sendResponseLogin().getVersion());
    this.entityNameRequest.setLoggedInUserId(this.loginComponent.sendResponseLogin().getUserId());
    this.entityNameRequest.setRoles(this.loginComponent.sendResponseLogin().getRoles());
  
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.entityNameRequest),this.options);
   }
}
