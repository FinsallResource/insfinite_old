//-------------------------Created by Suparno Saha-------------------------------//
export class RequestApplication{
    private clientId: String;

    private loggedInUserId: String;

    private serviceName: String;

    private version: String;

    private serviceMethod: String;

    private roles:String;

    public getRoles(): String {
        return this.roles;
    }

    public setRoles(roles: String): void {
        this.roles = roles;
    }


    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getLoggedInUserId(): String {
        return this.loggedInUserId;
    }

    public setLoggedInUserId(loggedInUserId: String): void {
        this.loggedInUserId = loggedInUserId;
    }

    public getServiceName(): String {
        return this.serviceName;
    }

    public setServiceName(serviceName: String): void {
        this.serviceName = serviceName;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

    public getServiceMethod(): String {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: String): void {
        this.serviceMethod = serviceMethod;
    }

}