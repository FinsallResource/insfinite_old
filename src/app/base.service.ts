import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UploadFile } from './model/UploadFile';
import { LoginComponent } from './login/login.component';

@Injectable({
  providedIn: 'root'
})
//-------------------------Created by Suparno Saha-------------------------------//
//this service consist of the constants which are used in multiple places and needs to be changed regularly 
export class BaseService {

  constructor() { }

 //key for hashing password
  key:string="F1NS@ll@2o19w6";

  //secret key for validating capcha response
  //secretKey:string="6LdpAtMUAAAAAGPhCGnmsYd4bLIJG50JNja1-7tG";
  //prodCiteKey:string="6LeZIdMUAAAAACtkbCaIOoqB2DmeGKxrq3JpRblq";
  //prodSecretKey:string="6LeZIdMUAAAAALpJOwVhn6m1Q4iblraWm-j3FXdR"

  clientId:string="2024";
  version:string="1.0.0";
 
  uploadUrl():string
  {
    return "https://cors-anywhere.herokuapp.com/http://213.136.87.68:8080/FinsAllServer/upload";
  }
  
  //
  baseUrl():string
  {
    //baseURL for running locally in Finsall.co.in "https://cors-anywhere.herokuapp.com/https://www.finsall.co.in/FinsAllServer/api";

    //baseURL for running in server in Finsall.co.in= 'https://www.finsall.co.in/FinsAllServer/api';

    //baseURL for running locally in Contavo "https://cors-anywhere.herokuapp.com/http://213.136.87.68:8080/FinsAllServer/api"

    //baseURL for running in server in Contavo "http://213.136.87.68:8080/FinsAllServer/api"
  
     //return  "https://cors-anywhere.herokuapp.com/https://www.finsall.co.in/FinsAllServer/api";
  

//   var URL=window.location.href; 
//   console.log(URL);
// var indexOfValue = URL.indexOf("www");
// if(indexOfValue > 0)
// var SERVER_URL = 'https://www.finsall.com/FinsAllServer/api';
// else
// var SERVER_URL = 'https://finsall.com/FinsAllServer/api';
//    return  SERVER_URL;

   return "https://cors-anywhere.herokuapp.com/http://213.136.87.68:8080/FinsAllServer/api";

  // return "http://213.136.87.68:8080/FinsAllServer/api";

   //return "https://cors-anywhere.herokuapp.com/http://192.168.19.165:8080/FinsAllServer/api";
  }

  
}


