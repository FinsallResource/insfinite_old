export class User_policy_coverage{
    private userPolicyCoverageId:string;

    private payOutFrequency:string;

    private payOutAmount:string;

    public coverageType:string;

    public getUserPolicyCoverageId(): string {
        return this.userPolicyCoverageId;
    }

    public setUserPolicyCoverageId(userPolicyCoverageId: string): void {
        this.userPolicyCoverageId = userPolicyCoverageId;
    }

    public getPayOutFrequency(): string {
        return this.payOutFrequency;
    }

    public setPayOutFrequency(payOutFrequency: string): void {
        this.payOutFrequency = payOutFrequency;
    }

    public getPayOutAmount(): string {
        return this.payOutAmount;
    }

    public setPayOutAmount(payOutAmount: string): void {
        this.payOutAmount = payOutAmount;
    }

}