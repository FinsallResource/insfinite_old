export class UploadFile{
    private file:File;
    private userId:string;
    private referenceId:string;
    private typeId:string;
    private nameId:string;
    private label:string;

    public getLabel(): string {
        return this.label;
    }

    public setLabel(label: string): void {
        this.label = label;
    }

    public getFile(): File {
        return this.file;
    }

    public setFile(file: File): void {
        this.file = file;
    }

    public getUserId(): string {
        return this.userId;
    }

    public setUserId(userId: string): void {
        this.userId = userId;
    }

    public getReferenceId(): string {
        return this.referenceId;
    }

    public setReferenceId(referenceId: string): void {
        this.referenceId = referenceId;
    }

    public getTypeId(): string {
        return this.typeId;
    }

    public setTypeId(typeId: string): void {
        this.typeId = typeId;
    }

    public getNameId(): string {
        return this.nameId;
    }

    public setNameId(nameId: string): void {
        this.nameId = nameId;
    }

}