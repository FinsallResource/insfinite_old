import { Component, OnInit, Inject } from '@angular/core';
import { PolicyDataService } from '../policy-data.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { User_policy } from '../model/User_policy';
import { EditPolicyComponent } from '../edit-policy/edit-policy.component';

@Component({
  selector: 'app-policy-details',
  templateUrl: './policy-details.component.html',
  styleUrls: ['./policy-details.component.css']
})
export class PolicyDetailsComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<PolicyDetailsComponent>,private policydata:PolicyDataService, 
    @Inject(MAT_DIALOG_DATA) public data:string[],public dialog: MatDialog,private router: Router,
    private userPolicy:User_policy)
    {
    }

    onNoClick(): void {
      this.dialogRef.close();}

    editDialog():void{
      const dialogRef = this.dialog.open(EditPolicyComponent, {
        panelClass: 'my-dialog',
        width: '650px',
        data: this.policydata.policy
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
        
          // DO SOMETHING
          
        }
      });
    }

  ngOnInit() {
    console.log(this.data);
  }

}
