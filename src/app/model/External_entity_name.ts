export class External_entity_name{
    private website: string;

    private externalEntityNameId: string;

    private entityName: string;

    private icon: string

    private telephone: string;

    public getWebsite(): string {
        return this.website;
    }

    public setWebsite(website: string): void {
        this.website = website;
    }

    public getExternalEntityNameId(): string {
        return this.externalEntityNameId;
    }

    public setExternalEntityNameId(externalEntityNameId: string): void {
        this.externalEntityNameId = externalEntityNameId;
    }

    public getEntityName(): string {
        return this.entityName;
    }

    public setEntityName(entityName: string): void {
        this.entityName = entityName;
    }

    public getIcon(): string {
        return this.icon;
    }

    public setIcon(icon: string): void {
        this.icon = icon;
    }

    public getTelephone(): string {
        return this.telephone;
    }

    public setTelephone(telephone: string): void {
        this.telephone = telephone;
    }

}