import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginRequest } from './model/loginRequest';
import { AuthGuardService } from './auth-guard.service';
import { Observable } from 'rxjs';
import { LoginResponse } from './model/loginResponse';
import { BaseService } from './base.service';
import { timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
//*********************Created by Suparno Saha*************************//
//service for validation of credentials in login
export class ValidateLoginService {


  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
 .set('Authentication-is-public','Y')
  };

  
  validate(genericParam: String, password: String)
 {
  this.login.setServiceName("UserService");
  this.login.setGenericParam(genericParam);
  this.login.setIsPassword(true);
  this.login.setPassword(password);
  this.login.setServiceMethod("login");
  this.login.setRoles("customer");
  this.login.setClientId(this.baseService.clientId);
  this.login.setVersion(this.baseService.version);
  return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.login),this.options)
  .pipe(
    catchError((err: HttpErrorResponse) => {
      if (err.status == 404) {
        this.router.navigate(["noInternet"]);
        return null;
      } 
    })
  );
  // .pipe(timeout(2000));
  
 }
  constructor(private http: HttpClient,private login:LoginRequest,  private response: LoginResponse,
    private router: Router, private authGuard: AuthGuardService,private baseService:BaseService ) { }
}


