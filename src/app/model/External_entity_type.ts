import { Insurance_type } from './Insurance_type';

export class External_entity_type{
    private  insurance_type:Insurance_type;

    public externalEntityTypeId:string;

    private entityType:string;

    private icon:string;

    public getInsurance_type(): Insurance_type {
        return this.insurance_type;
    }

    public setInsurance_type(insurance_type: Insurance_type): void {
        this.insurance_type = insurance_type;
    }

    public getExternalEntityTypeId(): string {
        return this.externalEntityTypeId;
    }

    public setExternalEntityTypeId(externalEntityTypeId: string): void {
        this.externalEntityTypeId = externalEntityTypeId;
    }

    public getEntityType(): string {
        return this.entityType;
    }

    public setEntityType(entityType: string): void {
        this.entityType = entityType;
    }

    public getIcon(): string {
        return this.icon;
    }

    public setIcon(icon: string): void {
        this.icon = icon;
    }

}