import { RequestApplication } from './requestApplication';

export class RequestEntityName extends RequestApplication{

    private genericIdentifiers:string[];

    public getGenericIdentifiers(): string[] {
        return this.genericIdentifiers;
    }

    public setGenericIdentifiers(genericIdentifiers: string[]): void {
        this.genericIdentifiers = genericIdentifiers;
    }

}