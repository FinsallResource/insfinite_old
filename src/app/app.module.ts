import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { NgModule, ChangeDetectorRef, ElementRef } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import {MatCheckboxModule, MatTableModule, MatIconModule, 
  MatButtonModule, MatPaginatorModule, MatFormFieldModule, MatInputModule, 
  MatSelectModule, MatToolbarModule, MatSidenavModule, MatListModule, MatSnackBarModule,
   MatSortModule,GestureConfig,MatStepperModule,MatAutocompleteModule } from '@angular/material';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule} from '@angular/material/menu';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginResponse } from './model/loginResponse';
import { ValidateLoginService } from './validate-login.service';
import { LoginRequest } from './model/loginRequest';
import { AuthGuardService } from './auth-guard.service';
import { BaseService } from './base.service';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { SignupComponent } from './signup/signup.component';
import { PoliciesComponent } from './policies/policies.component';
import { RequestApplication } from './model/requestApplication';
import { ResponsePolicies } from './model/ResponsePolicies';
import { User_policy } from './model/User_policy';
import { User_policy_agent } from './model/User_policy_agent';
import { User_policy_coverage } from './model/User_policy_coverage';
import { User_policy_document } from './model/User_policy_document';
import { User_relation } from './model/User_relation';
import { Insurance_type_root } from './model/Insurance_type_root';
import { Insurance_type } from './model/Insurance_type';
import { Insurance_type_and_root } from './model/Insurance_type_and_root';
import { External_entity_type } from './model/External_entity_type';
import { External_entity_name } from './model/External_entity_name';
import { LayoutModule } from '@angular/cdk/layout';
import { PolicyDetailsComponent } from './policy-details/policy-details.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { PolicyDataService } from './policy-data.service';
import {MatDialogModule, MatDialogRef,MAT_DIALOG_DATA,MatDialogConfig} from '@angular/material/dialog';
import { ArchiveService } from './archive.service';
import { ArchivePolicy } from './model/ArchivePolicy';
import { FileSelectDirective } from 'ng2-file-upload';
import { AddPolicyComponent } from './add-policy/add-policy.component';
import { RequestEntityName } from './model/RequestEntityName';
import { CdkStepper, CdkStepperModule } from '@angular/cdk/stepper';
import { FileUploadModule } from 'ng2-file-upload';
import { UploadFile } from './model/UploadFile';
import { ArchivesComponent } from './archives/archives.component';
import { SharePolicyComponent } from './share-policy/share-policy.component';
import { EditPolicyComponent } from './edit-policy/edit-policy.component';
import { DatePipe } from '@angular/common';
import { RequestVehicleBrand } from './model/RequestVehicleBrand';
import { RequestVehicleModel } from './model/RequestVehicleModel';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    PoliciesComponent,
    PolicyDetailsComponent,
    ConfirmationDialogComponent,
    AddPolicyComponent,
    ArchivesComponent,
    SharePolicyComponent,
    EditPolicyComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatPaginatorModule,
    MatFormFieldModule,
   FormsModule,
   ReactiveFormsModule,
   MatInputModule,
   MatSelectModule,
   BrowserAnimationsModule,
   MatToolbarModule,
   MatSidenavModule,
   MatListModule,
   MatMenuModule,
   MatFormFieldModule,
   MatStepperModule,
   MatAutocompleteModule,
   CdkStepperModule,
   FileUploadModule
  ],
  entryComponents: [ ConfirmationDialogComponent,PolicyDetailsComponent,SharePolicyComponent],
  exports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatCheckboxModule,
    FormsModule,
    MatSnackBarModule,
    MatSortModule
  ],
  providers: [LoginResponse,ValidateLoginService,AuthGuardService,BaseService,HttpClient,LoginRequest,
  LoginComponent,RequestApplication,ResponsePolicies,User_policy,User_policy_agent,User_policy_coverage,
User_policy_document,User_relation,Insurance_type_root,Insurance_type,Insurance_type_and_root,External_entity_type,
External_entity_name,{provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig},PolicyDataService,PolicyDetailsComponent,
{provide: MatDialogRef,useValue: {}},{provide: MAT_DIALOG_DATA,useValue: {}},ArchiveService,ArchivePolicy,
{provide: MatDialogConfig,useValue: {}},RequestEntityName,CdkStepper,UploadFile,RequestVehicleBrand,RequestVehicleModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
