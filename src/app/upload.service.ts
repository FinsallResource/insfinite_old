import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UploadFile } from './model/UploadFile';
import { Router } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient, private upload:UploadFile,
    private router: Router, private loginComponent:LoginComponent, private baseService:BaseService) { }

    private options = { headers: new HttpHeaders()
    .set('Authentication-is-public','Y')
  }
  uploadFile(fileItem:FormData)
  {
    // this.upload.setFile(file);
    // this.upload.setNameId(nameId);
    // this.upload.setTypeId(typeId);
    // this.upload.setReferenceId("");
    // this.upload.setUserId(this.loginComponent.sendResponseLogin().getUserId());
    // this.upload.setLabel("Policy");
     console.log(fileItem.getAll('referenceId'));
    
    return this.http.post(this.baseService.uploadUrl(),fileItem,this.options);
  }
}
