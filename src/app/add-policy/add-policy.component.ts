import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { External_entity_type } from '../model/External_entity_type';
import { ExternalEntityService } from '../external-entity.service';
import { External_entity_name } from '../model/External_entity_name';
import { MatHorizontalStepper, MatStep } from '@angular/material';
import {  FileUploader } from 'ng2-file-upload';
import { BaseService } from '../base.service';
import { LoginComponent } from '../login/login.component';
import { UploadService } from '../upload.service';

@Component({
  selector: 'app-add-policy',
  templateUrl: './add-policy.component.html',
  styleUrls: ['./add-policy.component.css']
})
export class AddPolicyComponent implements OnInit {
  

  constructor( public dialogRef: MatDialogRef<AddPolicyComponent>,
    @Inject(MAT_DIALOG_DATA) public data:string[],public dialog: MatDialog,private router: Router,
    private entityType:External_entity_type,private _formBuilder: FormBuilder, 
    private externalService:ExternalEntityService, private entityName:External_entity_name,
    private uploadService:UploadService, private loginComponent:LoginComponent,public snackbar:MatSnackBar)
    {

    }

    loading: boolean=false;
    isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  insuranceRoot:string[];
  nameAvailable= false;
  step1Completed=false;
  step2Completed=false;
  nameIdAvailable= false;
  typeId:string;
  nameId:string;
  
  
  @ViewChild(MatHorizontalStepper,{static: false}) private stepper: MatHorizontalStepper;

  onNoClick(): void {
    this.dialogRef.close();}

    public uploader: FileUploader = new FileUploader({});

    openSnackBar(message: string, action: string) {
      this.snackbar.open(message, action, {
         duration: 5000,
      });
    }

    fileUpload()
    {

         let fileItem= this.uploader.queue[0]._file;
         console.log(fileItem.name);
         
         const formData = new FormData();
    formData.append('file', fileItem);
    formData.append('userId',this.loginComponent.sendResponseLogin().getUserId());
    formData.append('nameId',this.nameId);
    formData.append('typeId',this.typeId);
    formData.append('label',"Policy");
    let now = new Date().getTime();
    formData.append('referenceId',now.toString());

         this.uploadService.uploadFile(formData).subscribe((data1:any)=>
         {
           console.log(data1);
           this.uploader.clearQueue();
           this.openSnackBar(data1.successMessage,"ok");
           this.onNoClick();
         })

 }

 

    setEntityName(name: any){
      this.step2Completed=true;
      this.nameId=name.externalEntityNameId;
      console.log( name.externalEntityNameId+"---"+name.entityName);
      this.stepper.next();
      this.nameIdAvailable= true;
      
    }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.loading=true;
    this.externalService.getExternalType().subscribe((data:any)=>
    {
      console.log(data);
      this.entityType=data.external_entity_type;
      this.loading=false; 
    })
    

  }

  getEntityName(type:any)
  {
    // console.log(stepper);
    this.step1Completed=true;
  
    this.loading=true;
    this.typeId=type.externalEntityTypeId;
    this.insuranceRoot=[];
     console.log(type);
     for(let event of type.insurance_type.insurance_type_and_root)
     {
       this.insuranceRoot.push(event.insurance_type_root.insuranceTypeRootId);
     }
     console.log(this.insuranceRoot);
     this.externalService.getExternalName(this.insuranceRoot).subscribe((data:any)=>
     {
       console.log(data);
       this.entityName=data.external_entity_name.external_entity_name_list;
       this.loading=false;
       this.stepper.next();
       this.nameAvailable=true;
       this.nameIdAvailable=false;
       this.step2Completed=true;
       
     })
     
  }
  }


