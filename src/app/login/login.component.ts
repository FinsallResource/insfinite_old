import { Component, OnInit, ViewChild, HostListener, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LocationStrategy } from '@angular/common';
import { sha256} from 'js-sha256';
import { BaseService } from '../base.service';
import { LoginResponse } from '../model/loginResponse';
import { AuthGuardService } from '../auth-guard.service';
import { ValidateLoginService } from '../validate-login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor(private formBuilder: FormBuilder,
    private response: LoginResponse, private router:Router,private auth:AuthGuardService,
    private locationStrategy: LocationStrategy,
    private baseService:BaseService,
    private validate: ValidateLoginService) {}

  mobileNo: string= "";
 password: string= "";
 isValid: boolean= false; 
 clientId: string="2017";
 isRequired: boolean=false;
 errorMessage:String='';
 isLoggedOut: boolean=false;
 loading = false;
 registerForm: FormGroup;
 key:string=this.baseService.key;
 text:string= "Email/Mobile Number";
 
 UsernameFormControl = new FormControl('', [
  Validators.required,
  Validators.pattern("([_a-z0-9]+(\\.[_a-z0-9]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,5}))|(\\d+$)$"),
]);

PasswordFormControl = new FormControl('', [
  Validators.required,]);


 fillRequired()
 {
this.isRequired=true;
 }
//restricts browser back key from getting into previous page 
 @HostListener('window:popstate', ['$event'])
  onPopState(event:any) {
    this.router.navigate(["login"]);
    
  }

 onEnteringMobileNo(event: any)
 {
   this.mobileNo=event.target.value;
   
   this.isRequired=false;
 }

 onEnteringPassword(event: any)
 {
   this.password=event.target.value;
   this.isRequired=false;
   this.password=(sha256.hmac(this.key, this.password));
  
   
 }


//method get called on button press (call the login API)
 check(){
         //this.response=this.validate.validateWithMobileNo(this.mobileNo,this.password);
         this.loading=true;
         this.validate.validate(this.mobileNo,this.password).subscribe((data:any) => 
      {
        
        this.response.setClientId((data.clientId));
        this.response.setPassword((data.password));
        this.response.setVersion((data.version));
        this.response.setUserId((data.userId));
        this.response.setUserName((data.userName));
        this.response.setLastName((data.lastName));
        this.response.setFirstName((data.firstName));
        this.response.setEmailId((data.emailId));
        this.response.setMobileNo((data.mobileNo));
        this.response.setPan((data.pan));
        this.response.setRoleId((data.roleId));
        this.response.setCountry((data.country));
        this.response.setErrorMessage((data.errorMessage));
        this.response.setRoles(data.roles);
        this.loading=false;
        console.log(this.response);

       if (this.response.getErrorMessage() == undefined)
     {
       
      this.router.navigate(['policies']);
      }
      else{
        
        this.errorMessage=this.response.getErrorMessage();
        this.isValid=true;
      }
      
     },error => console.debug('ERROR', error) )
    
        }

  sendResponseLogin(){
    return this.response;
  }
 getResponseLogin(res:LoginResponse){

  this.response=res;
  this.isLoggedOut=true;
 }

 preventBackButton() {
  history.pushState(null, null, location.href);
  this.locationStrategy.onPopState(() => {
    history.pushState(null, null, location.href);
  })
}
  
get f() { return this.registerForm.controls; }

  ngOnInit() {
   
  }

}
